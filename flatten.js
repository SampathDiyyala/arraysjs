const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

function flatten(elements) {
    let flatArr=[]
    for(let i=0;i<elements.length;i++){
        if(Array.isArray(elements[i])){
            flatArr=flatArr.concat(flatten(elements[i]))
        }
        else{
            flatArr.push(elements[i])
        }
    }
    return flatArr
}
console.log(flatten(nestedArray))
module.exports=flatten